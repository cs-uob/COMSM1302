# COMSM1302 Overview of Computer Architecture

This is the repository and website for the COMSM1302 'Overview of Computer Architecture' unit at the University of Bristol for the conversion MSc in the 2019-20 academic year.

If you are browsing the repository, you can find the website at https://cs-uob.gitlab.io/COMSM1302.

See the menu on the left for links to different pages.
